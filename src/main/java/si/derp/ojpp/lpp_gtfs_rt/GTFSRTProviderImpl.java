package si.derp.ojpp.lpp_gtfs_rt;

import com.google.transit.realtime.GtfsRealtime.*;
import com.google.transit.realtime.GtfsRealtime.TripUpdate.StopTimeEvent;
import com.google.transit.realtime.GtfsRealtime.TripUpdate.StopTimeUpdate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.onebusway.gtfs_realtime.exporter.GtfsRealtimeExporterModule;
import org.onebusway.gtfs_realtime.exporter.GtfsRealtimeLibrary;
import org.onebusway.gtfs_realtime.exporter.GtfsRealtimeMutableProvider;
import org.onebusway.gtfs_realtime.exporter.GtfsRealtimeProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static si.derp.ojpp.lpp_gtfs_rt.GTFSRTProxyMain.LPP_API_KEY;

/**
 * This class produces GTFS-realtime trip updates and vehicle positions by
 * periodically polling the custom LPP API and converting the resulting
 * vehicle and stop data into the GTFS-realtime format.
 *
 * Since this class implements {@link GtfsRealtimeProvider}, it will
 * automatically be queried by the {@link GtfsRealtimeExporterModule} to export
 * the GTFS-realtime feeds to file or to host them using a simple web-server, as
 * configured by the client.
 */
@Singleton
public class GTFSRTProviderImpl {
    private final URL busesURL = new URL("https://data.lpp.si/api/bus/bus-details?trip-info=1");
    private final String arrivalsUrlTemplate = "https://data.lpp.si/api/route/arrivals-on-route?trip-id=";

    /**
     * How often vehicle data will be downloaded, in seconds.
     */
    final private int _refreshInterval = 10;

    /**
     * How long (in seconds) should we wait before re-downloading arrival data for a subroute
     */
    final private int _arrivalRefreshTime = 30;

    private static final Logger _log = LoggerFactory.getLogger(GTFSRTProviderImpl.class);
    /**
     * A JSON object key to use for including arrival data into vehicle objects during download.
     * This MUST NOT conflict with a field from the LPP API!
     */
    private static final String KEY_TRIP_STATIONS = "__trip_stations_arrivals";

    private ScheduledExecutorService _scheduler;

    private GtfsRealtimeMutableProvider _gtfsRealtimeProvider;
    private final Gtfs gtfs = new Gtfs();


    public GTFSRTProviderImpl() throws MalformedURLException {}

    @Inject
    public void setGtfsRealtimeProvider(GtfsRealtimeMutableProvider gtfsRealtimeProvider) {
        _gtfsRealtimeProvider = gtfsRealtimeProvider;
    }

    /**
     * The start method automatically starts up a recurring task that periodically
     * downloads the latest data from the upstream API and processes them.
     */
    @PostConstruct
    public void start() throws MalformedURLException {
        _log.info("Starting GTFS-realtime service");
        _scheduler = Executors.newSingleThreadScheduledExecutor();
        _scheduler.scheduleAtFixedRate(new GtfsDataUpdater(this.gtfs), 0, 30, TimeUnit.MINUTES);
        _scheduler.scheduleAtFixedRate(new VehiclesRefreshTask(), 0, _refreshInterval, TimeUnit.SECONDS);
    }

    /**
     * The stop method cancels the recurring vehicle data downloader task.
     */
    @PreDestroy
    public void stop() {
        _log.info("Stopping GTFS-realtime service");
        _scheduler.shutdownNow();
    }

    /**
     * This method downloads the latest downloaded data, processes each vehicle in
     * turn, and creates a GTFS-realtime feed of trip updates and vehicle positions
     */
    private void refreshVehicles() throws IOException, JSONException {

        List<JSONObject> vehicleArray = downloadVehicleDetails();


        FeedMessage.Builder tripUpdates = GtfsRealtimeLibrary.createFeedMessageBuilder();
        FeedMessage.Builder vehiclePositions = GtfsRealtimeLibrary.createFeedMessageBuilder();


        for (JSONObject bus : vehicleArray) {
            if (!bus.has(KEY_TRIP_STATIONS))
                continue;


            long now = System.currentTimeMillis() / 1000;


            /*
                TRIP MATCHING
             */

            TripDescriptor.Builder tripDescriptor = TripDescriptor.newBuilder();

            JSONArray stations = bus.getJSONArray(KEY_TRIP_STATIONS);

            // Find the closest matching trip for every stop position and save the one that appears the most times
            Map<String, Integer> bestTrips = new HashMap<>();
            for (Object o1 : stations) {
                JSONObject station = (JSONObject) o1;
                for (Object o2 : station.getJSONArray("arrivals")) {
                    JSONObject arrivalObj = (JSONObject) o2;
                    long timestamp = now + arrivalObj.getInt("eta_min") * 60L;
                    String stationCode = Integer.toString(station.getInt("station_code"));
                    LocalDateTime time = Instant.ofEpochSecond(timestamp).atZone(ZoneId.of("Europe/Ljubljana")).toLocalDateTime();
                    String tripId = this.gtfs.getTripId(bus.getString("trip_id"), stationCode, time);
                    if (tripId != null)
                        bestTrips.put(tripId, bestTrips.getOrDefault(tripId, 0) + 1);
                }
            }
            int max = -1;
            for (Map.Entry<String, Integer> entry : bestTrips.entrySet()) {
                if (entry.getValue() > max) {
                    max = entry.getValue();
                    tripDescriptor.setTripId(entry.getKey());
                }
            }
            if (max == -1) {
                _log.error("Couldn't match trip for bus!");
            }


            VehicleDescriptor.Builder vehicleDescriptor = VehicleDescriptor.newBuilder();
            String vehicle_id = bus.getString("bus_unit_id");
            vehicleDescriptor.setId(vehicle_id);


            /*
                ARRIVAL (ETA) UPDATES
             */

            TripUpdate.Builder tripUpdate = TripUpdate.newBuilder();

            tripUpdate.setTrip(tripDescriptor);
            tripUpdate.setVehicle(vehicleDescriptor);

            for (Object o1 : stations) {
                JSONObject station = (JSONObject) o1;
                for (Object o2 : station.getJSONArray("arrivals")) {
                    JSONObject arrivalObj = (JSONObject) o2;

                    StopTimeEvent.Builder arrival = StopTimeEvent.newBuilder();
                    long timestamp = now + arrivalObj.getInt("eta_min") * 60L;
                    arrival.setTime(timestamp);

                    StopTimeUpdate.Builder stopTimeUpdate = StopTimeUpdate.newBuilder();
                    stopTimeUpdate.setArrival(arrival);
                    String stationCode = Integer.toString(station.getInt("station_code"));
                    stopTimeUpdate.setStopId(stationCode);

                    tripUpdate.addStopTimeUpdate(stopTimeUpdate);
                }
            }

            FeedEntity.Builder tripUpdateEntity = FeedEntity.newBuilder();
            tripUpdateEntity.setId(vehicle_id);
            tripUpdateEntity.setTripUpdate(tripUpdate);
            tripUpdates.addEntity(tripUpdateEntity);

            /*
                POSITION (GPS) UPDATES
             */

            double lat = bus.getDouble("coordinate_y");
            double lon = bus.getDouble("coordinate_x");

            Position.Builder position = Position.newBuilder();
            position.setLatitude((float) lat);
            position.setLongitude((float) lon);

            VehiclePosition.Builder vehiclePosition = VehiclePosition.newBuilder();
            vehiclePosition.setPosition(position);
            vehiclePosition.setTrip(tripDescriptor);
            vehiclePosition.setVehicle(vehicleDescriptor);
            // TODO: vehiclePosition.setTimestamp();


            FeedEntity.Builder vehiclePositionEntity = FeedEntity.newBuilder();
            vehiclePositionEntity.setId(vehicle_id);
            vehiclePositionEntity.setVehicle(vehiclePosition);
            vehiclePositions.addEntity(vehiclePositionEntity);

        }

        _gtfsRealtimeProvider.setVehiclePositions(vehiclePositions.build());

        if (tripUpdates.getEntityCount() > 0)
            _gtfsRealtimeProvider.setTripUpdates(tripUpdates.build());
    }


    private List<JSONObject> downloadVehicleDetails() throws IOException, JSONException {
        List<JSONObject> vehiclesArray = new ArrayList<>();

        // Get bus data from API
        URLConnection con = busesURL.openConnection();
        con.setRequestProperty("apikey", LPP_API_KEY);
        con.connect();
        BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
        JSONTokener tokener = new JSONTokener(reader);
        JSONArray jsonVehicles = new JSONObject(tokener).getJSONArray("data");

        // Filter out inactive buses
        for (Object o : jsonVehicles) {
            JSONObject bus = (JSONObject) o;
            if (bus.isNull("trip_id"))
                continue;
            vehiclesArray.add(bus);
        }

        _log.info("Downloaded " + vehiclesArray.size() + " vehicle positions");

        // For each bus, find arrivals on route
        for (JSONObject vehicle : vehiclesArray) {
            JSONArray stations = getArrivals(vehicle);
            vehicle.put(KEY_TRIP_STATIONS, stations);
        }

        return vehiclesArray;
}

    Map<String, JSONArray> trips_stations_arrivals = new HashMap<>();
    Map<String, Long> trips_stations_arrivals_last_update = new HashMap<>();

    private JSONArray getArrivals(JSONObject vehicle) throws IOException {
        String subroute_id = vehicle.getString("trip_id");
        if (System.currentTimeMillis() - trips_stations_arrivals_last_update.getOrDefault(subroute_id, 0L) < _arrivalRefreshTime*1000) {
            return trips_stations_arrivals.get(subroute_id);
        } else {
            URL url = new URL(arrivalsUrlTemplate + subroute_id);
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            JSONTokener tokener = new JSONTokener(reader);
            JSONArray stationArray = new JSONObject(tokener).getJSONArray("data");
            trips_stations_arrivals.put(subroute_id, stationArray);
            return stationArray;
        }
    }

    /**
 * Task that will download new vehicle data from the remote data source when
 * executed.
 */
private class VehiclesRefreshTask implements Runnable {

    @Override
    public void run() {
        try {
            _log.info("refreshing vehicles");
            refreshVehicles();
        } catch (Exception ex) {
            _log.error("Error in vehicle refresh task", ex);
            try {
                throw ex;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

}
