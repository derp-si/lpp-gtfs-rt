FROM maven:3.5-jdk-11-slim as build

# Copy POM and download dependencies
ADD pom.xml .
RUN mvn dependency:resolve-plugins dependency:go-offline

# Actually build
ADD . .
RUN mvn package

FROM openjdk:11-jre-slim

EXPOSE 80

COPY --from=build target/lpp-gtfs-rt-proxy-*.jar .

CMD java -jar lpp-gtfs-rt-proxy-*.jar \
    --lppApiKey=${LPP_API_KEY} \
    --tripUpdatesUrl=http://0.0.0.0:80/trip-updates \
    --vehiclePositionsUrl=http://0.0.0.0:80/vehicle-positions
